## Contrôler son environnement

### Variables et options de l'environnement de travail

Les variables correspondent à des emplacements dans la mémoire de l'ordinateur. Associer nom et valeur permet de traiter les données de manière symbolique.  

*Variables d'environnement*: variables créées et gérées par le shell (en opposition à celles créées par l'utilisateur = "variables locales"). Elles ont des noms prédéfinis en lettres capitales et sont renseignées par le shell lorsque l'utilisateur se connecte au système.  

Les variables d'environnement sont relatives à chaque utilisateur et chaque session, ne peuvent être exploitées et utilisées que dans la session courante.  
Ces variables sont initialisées (renseignées) par le shell mais leur valeur est modifiable, ces modifications influent sur le comportement du shell lors de la session courante.  

cf p.150, variables importantes.  

``PATH`` donne l'accès aux commandes du système, sert à retrouver les commandes dans l'arborescence sans fournir le chemin absolu. C'est la liste des répertoires dans lesquels le shell cherche les commandes (dans l'ordre de la liste, cf p.140).  
Consultation du contenu de la variable avec ``echo $PATH``.  

La variable ``HOME`` contient le chemin d'accès du répertoire personnel de l'utilisateur, ``cd`` sans argument utilise ``HOME``.  

On peut modifier le contenu d'une variable d'environnement, sera valable pour toute la session, cf p.150-151.  

Le comportement du bash se modifie/configure à l'aide des options du shell. La commande ``shopt`` (shell option) positionne les options, sans argument, liste les options disponibles du shell et montre si elles sont positionnées ou non, cf p.151.  
Activation d'une option à l'aide de ``shopt -s`` (set) et inactivation à l'aide de ``shopt -u`` (unset).  

#### L'environnement de travail

La commande ``env`` sans argument (idem que ``printenv``) liste les variables de l'environnement courant, l'ajout d'argument(s) permet de modifier leur valeur, *ex:*  
``env variable1=valeur1 variable2=valeur2 cmd opt arg``  
Exécution de la commande ``cmd`` avec l'option ``opt`` et l'argument ``arg`` en modifiant/étendant l'environnement de travail actuel avec les variables ``variable1`` et ``variable2``. Cette extension/modification **ne sera valable que pour l'exécution de la commande** ``cmd``, cf exemple p.152-153.  

La commande ``set`` sans argument fournit les variables d'environnement et celles créées dans la session courante, ``unset`` supprime une variable de l'environnement.  
``set -o`` permet de lister les options contrôlées par la commande ``set``.  

La commande ``declare`` sans argument a le même rôle que ``set`` mais différencie les variables exportées des variables locales.  

*Variable locale*: variable créée par l'utilisateur dans un shell. N'est connue que dans le shell qui l'a créée.  
*Sous-shell*:shell lançé à partir d'un autre shell. Les variables d'environnement du shell original sont transmises au sous-shell &rightarrow; ont les mêmes valeurs.  

La commande ``bash`` lance le programme bash dans le shell original, dans le sous-shell vérification de la valeur des variables d'environnement exportées.  
Lorsqu'on crée une commande dans le shell original, pour la rendre accessible dans le sous-shell, il faut l'ajouter aux variables d'environnement et la transformer (promouvoir) en variable d'environnement. Cet ajout et transformation se font à l'aide de la commande ``export cmd``, cf exemple p.154-155.  

#### Les fichiers de configuration du shell

Lorsqu'on s'identifie/ouvre un terminal, des scripts shell spécifiques sont exécutés, ce sont des fichiers contenant des commandes shell à exécuter.  

**/etc/profile et ~/.profile:**  

Le fichier ``/etc/profile`` est un script exécuté automatiquement à la connexion de tous les utilisateurs, il définit les variables et paramètres identiques pour tous les utilisateurs.  
Ce fichier est commun à plusieurs shell &rightarrow; ne pas y mettre de variable spécifique à bash (à mettre dans le fichier ``/etc/bashrc``).  

Le fichier ``~/.profile`` (ou ``/home/mylogin/.profile``) contrairement à ``/etc/profile`` n'est exécuté que lorsque l'utilisateur *mylogin* se connecte. Ce fichier est exécuté après ``/etc/profile`` &rightarrow; permet de modifier les variables défà définies dans ``/etc/profile`` ou d'en créer de nouvelles.  
Ce fichier est modifiable par l'utilisateur et contient les variables d'environnement qui lui sont spécifiques.  

**/etc/bashrc et /.bashrc:**  

Si l'utilisateur *mylogin* utilise le shell bash, le script ``/home/mylogin/.bashrc`` est exécuté à la suite des fichiers ``/etc/profile`` et ``~/.profile``.  
Le fichier ``/home/mylogin/.bashrc`` débute par des commandes conduisant à exécuter le fichier ``/etc/bashrc`` car contient des variables, fonctions et/ou alias propres à bash et communs à tous les utilisateurs. La suite du fichier ``.bashrc`` est complétée par les définitions des variables, fonctions et/ou alias spécifiques à l'utilisateur, cf exemple p.156.  

#### L'invite de commande et le terminal

L'invite de commande peut être modifiée afin de contenir les informations nous étant nécessaires. Ces informations pouvant être le contenu de variables d'environnement ou le résultat de l'exécution de commandes.  
La définition des informations affichées est spécifiée dans la variable d'environnement ``PS1``. Pour rendre ses modifications permanentes, elles doivent être sauvegardées dans un des fichiers de configuration du shell (comme ``/etc/profile``).  

cf p.157, commandes utilisées pour la modification de la valeur de ``PS1``.  

La commande ``bash`` dans un terminal permet de lancer un sous-shell, utile lorsqu'on souhaite modifier des variables d'environnement sans affecter celles du shell d'origine.  
L'outil ``tmux`` (terminal multiplexer) permet de mettre un shell entier en tâche de fond ou le ramener en avant-plan, cf exemple p.158-159.  

## Filtres simples

*Filtre*: moyen de simplifier, séparer, épurer ou clarifier un flux de données (souvent un texte ou une string). Les commandes lisant et écrivant sur les entrées/sorties sont appelées des filtres, ils sont souvent utilisés en branchement de commandes.

#### Découpage d'un fichier

Les commandes ``head`` et ``tail`` permettent d'afficher certaines lignes d'un fichier, l'utilisation de ces commandes en tant que filtres se fait le plus souvent ainsi: ``head -n 15 /var/log/syslog`` (idem avec ``tail``).  

La commande ``split`` permet de découper un fichier en plusieurs fichiers (de même taille si possible) qui seront nommés ``PREFIXEaa``, ``PREFIXEab``, ... La chaîne ``PREFIXE`` étant donnée en argument de la commande, *ex:*  

+ ``split -b 22 /var/log/syslog log_``: division du fichier ``/var/log/syslog`` en plusieurs fichiers de 22 octets qui seront appelés ``log_aa``, ``log_ab``...  
+ ``split -l 30 /var/log/syslog log_``: même division que précemment mais en séparant en fichiers ayant le même nombre de lignes (ici 30).  

#### Modification de l'affichage d'un fichier à l'écran

La commande ``sort`` affiche la stdout ou le contenu d'un fichier en triant les lignes, on peut choisir les colonnes qui seront les clefs du tri. Cette commande ne supprime pas d'information. Sans option, ``sort`` trie le fichier en argument dans l'ordre alphabétique, cf exemple p.152.  

+ L'option ``-n`` effectue un tri numérique
+ L'option ``-k`` sélectionne la colonne sur laquelle le tri est effectué
+ L'option ``-t`` permet de changer le séparateur de colonne
+ L'option ``-r`` inverse l'ordre

La combinaison des options ``-n`` et ``-k`` permet de trier une colonne numériquement, un tri sur plusieurs colonnes est également possible, cf exemple p.153.  

#### Extraction d'information

La commande ``cut`` permet l'extraction selon des options précisées. La commande ``cut -c1-2 fichier`` extrait les 2 premiers caractères de chaque ligne de ``fichier`` alors que ``cut -f1,5 fichier`` extrait les 2e et 5e colonnes.  
Le caractère de séparation par défaut est la tabulation et non l'espace. Il peut être modifié à l'aide de l'option ``-d``, *ex:* pour un fichier contenant des mots séparés par des espaces, ``cut -d " " -f 2 fichier`` (ou ``cat fichier | cut -d " " -f 2``): extraction de la 2e colonne après redéfinition du séparateur.  
cf exemple p.154, la redéfinition du séparateur permet de jouer avec la sortie de la commande.  
On peut spécifier un ensemble de commande avec ``-`` et/ou ``,``, *ex:* ``-f 1-3,5,6`` sélectionne les colonnes ``1,2,3,5,6``. Pour sélectionner toutes les colonnes en suivant une, utilisation de ``-f 3-``: sélection de toutes les colonnes à partir de la 3e.  

#### Assemblage

La commande ``cat`` permet la concaténation de plusieurs fichiers à la suite, ensuite affichés sur la stdout.

La commande ``paste`` permet de fusionner à la suite les colonnes contenues dans plusieurs fichiers, l'ordre étant important, cf exemple p.165.  
L'option ``-d`` permet de modifier le séparateur qui par défaut est la tabulation, *ex:* ``paste -d : fichier1 fichier2``.  

La commande ``join`` (similaire à ``paste``) permet de mixer plusieurs informations, issues de fichiers ou de la stdin, en fonction de mots-clefs, cf exemple p.155-156.  
``join -1 2 -2 1 fichier1 fichier2``:  

+ l'option ``-1 2`` stipule que dans le 1er fichier (``fichier1``), le champ définissant la relation est la 2e colonne
+ l'option ``-2 1`` signifie que dans le 2nd fichier (``fichier2``), le champ définissant la relation est la 1e colonne
Dans la sortie de la commande, chaque ligne **commencera** par la clef de relation.  

#### Modification du contenu

Commandes permettant de **modifier/supprimer** une information spécifique, le contenu de la stdin ou d'un fichier.  

La commande ``uniq`` permet de supprimer des **lignes adjacentes** identiques dans un fichier, cf exemple p.156.  
Les options les plus communes sont:  

+ ``-d`` affiche seulement les lignes ayant des doublons adjacents
+ ``-u`` affiche seulement les lignes n'ayant pas de doublon adjacent
+ ``-f`` permet d'ignorer un certain nombre de champs en début de chaque ligne avant suppression de doublons
+ ``-s`` permet d'ignorer un certain nombre de caractères en début de chaque ligne avant suppression de doublons
+ ``-c`` affiche le nombre de doublons adjacents rencontrés

La commande ``tr`` (translate) remplace une ligne de caractères par une autre, *ex:*  
``tr "[A-Z]" "[a-z]" < string`` changera toutes les majuscules en minuscules.  
``tr "Uf" "*F" string`` changera ``f`` par ``F`` et ``U`` par ``*``.  

#### Méta-information: wc

La commande ``wc`` est particulière, elle fournit des méta-informations sur un fichier ou la stdin, affiche le nombre de lignes (``-l``), mots (``-w``) et caractères (``-m``) contenus dans un fichier. L'option ``-c`` affiche le nombre d'octets.  

## Filtres puissants

#### La commande find

La commande ``find`` retrouve dans un répertoire/une liste de répertoires un fichier/ensemble de fichiers possédant certaines caractéristiques (nom, droits, dates, taille, etc...) ou satisfaisant à une expression donnée en argument, cf liste d'options p.172.  

*Ex:* ``find /etc /bin -name "a*"`` liste toutes les entités de ``/etc`` et ``/bin`` dont le nom commence par "a". Les guillements sont **essentiels** afin que le bash n'interprète pas le motif de recherche à la place de la commande ``find``.  
cf exemples p.172  

Il est possible de combiner les critères de recherche avec ``find``, il suffit de saisir toutes les options, ces critères sont pris comme des **intersections (ET)**, *ex:*  
``find . -size +15M -size -1000M`` liste toutes les entités du répertoire courant (``.``) dont le taille est > 15 Mébioctets et < 1 Gibioctet (l'unité des options ``-size`` doit être la même).  

L'option ``-o`` rend la recherche sur des **unions de critères (OU)**, *ex:*  
``find . -name "*.py" -o -name "*.cpp"`` liste toutes les entités du répertoire courant dont le nom se termine par ".py" ou ".cpp".  

La combinaison des unions et intersections est également possible, *ex:*  
``find . -name "*.py" -size +10k -o -iname "*.jpg" -size +10M`` liste dans le répertoire courant toutes les fichiers dont le nom se termine par ".py" et d'une taille > 10 kilo octets ou les fichiers dont le nom se termine par ".jpg" avec une taille > 10 Mébioctets.  

Une commande peut être appliquée au résultat de la recherche obtenue par ``find`` à l'aide de l'option ``-exec``. Elle utilise une syntaxe particulière: une ligne du résultat de la recherche est notée ``{}``, ainsi ``{}`` sera remplacée par le nom du fichier courant là ou elle apparaît dans l'argument de la commande à exécuter. L'option se termine par le caractère ``;``, pour empêcher son interprétation par le shell comme séparateur de commande, utilisation de l'inhibition de caractère ``\`` ou l'inhibition totale ``' '``, *ex:*  
``find . -iname "*.jpg" -exec echo "-- {} ++" \; ; echo $?``  
Chaque fichier est préfixé par ``--`` et suffixé par ``++``, le premier ``;`` indique la fin de la commande ``echo`` et le second ``;`` est le séparateur de commandes. La seconde commande de la ligne affiche le code retour de la dernière commande exécutée par ``-exec``.  

On peut enchaîner des actions avec plusieurs ``-exec`` et utiliser plusieurs fois ``{}`` dans la même commande, *ex:*  
``find . -name "*.txt" -exec wc -l {} ';' -exec cp {} {}.bak ';'``  
Trouve tous les fichiers ".txt" puis exécute la commande ``wc -l`` sur les entités trouvées et ensuite copie les entités trouvées dans des fichiers de même nom avec l'extension `` {}.bak``.  

La commande ``find`` peut également être enchaînée avec une autre commande, *ex:*  
``find . -name "*.txt" -exec cat {} ';' | wc -l``  
Trouve tous les fichiers ".txt" puis compte le nombre total de lignes pour les fichiers trouvés.  

#### La commande grep

La commande ``grep`` affiche toutes les lignes de la stdin/fichiers donnés en argument contenant une chaîne de caractères donnée en argument également, cf exemple p.164.  
Syntaxe: ``grep STRING fichier1 fichier2``, plusieurs fichiers pouvant être passés en argument.  

Les motifs recherchés par ``grep``sont des expressions régulières, des chaînes syntaxiques. Celles utilisées par ``grep`` permettent la recherche d'expressions complexes.  
La syntaxe utilisée pour les expressions régulières est présentée dans le manuel en ligne de re_format ou regex.  

L'accent circonflexe ``^`` signifie de chercher une expression en début de ligne, *ex:*  
``grep '^Li' fichier.txt`` renvoie toutes lignes commençant par ``Li``, l'expression régulière est passée entre simple quotes afin d'appliquer une inhibition totale.  

Le caractère ``$`` signifie de rechercher une expression en fin de ligne, *ex:*  
``grep 'ux$' fichier.txt`` renvoie toutes les lignes terminant par ``ux``.  

Les caractères ``[]`` indiquent qu'un ensemble de caractères est recherché dans un fichier, *ex:*  
``grep '^[KU]' fichier.txt`` affiche toutes les lignes commençant par "K" ou "U"  
``grep '^[K-U]' fichier.txt`` permet de définir une plage de caractères, toutes les lignes commençant par un caractère entre "K" et "U" seront affichées  

Options courantes:  

+ L'option ``-v`` inverse la sélection (lignes ne contenant pas la STRING)
+ L'option ``-n`` affiche les n° de ligne (selon celles du fichier original)
+ L'option ``-c`` donne le nombre de lignes trouvées
+ L'option ``-i`` enlève la sensibilité à la casse
+ L'option ``-w`` limite la recherche à un mot entier (délimité par des espaces ou situé en début/fin de phrase)
+ Les options ``-Ax, -Bx, -Cx`` (avec ``x`` valeur indiquant le nombre de ligne) permettent d'afficher les lignes:
	* ``-B`` avant ("before")
	* ``-A`` après ("after")
	* ``-C`` autour ("center")
+ L'option ``-E`` demande l'utilisation étendue de ``grep``, dans cette version la syntaxe des expressions régulières est enrichie par des caractères spéciaux comme ``|``  
Cf exemples p.176  

Dans une expression régulière, le ``.`` représente un caractère quelconque, *ex:*  
``grep "[A-Z].[v]" fichier`` liste toutes les lignes contenant une majuscule entre "A" et "Z" suivie d'un caractère quelconque et suivie d'un "v".  

Les accolades des expressions régulières permettent de spécifier des répétitions/un nombre de fois ce qui précède, *ex:*  
``grep '[R-Z][a-z]\{4\} ' fichier`` ici on recherche les lignes contenant une lettre majuscule entre "R" et "Z" puis 4 caractères minuscules entre "a" et "z" puis un espace, les accolades sont échappées.  
cf tableau p.177, synthèse des caractères spéciaux pour la construction d'expressions régulières.  

La commande ``grep`` s'utilise souvent en filtre, *ex:* ``cat f1 f2 f3 | grep "motif`` permet de recherche "motif" dans les fichiers en arguments.  

#### La commande awk

``awk`` est un langage de programmation, classé parmi les filtres programmables. C'est un outil de sélection et manipulation de texte comme ``grep``.  
Syntaxe: ``awk 'commandes awk' fichier``  

Les lignes du fichier sont traitées 1 à 1 de manière séquentielle, chaque ligne comporte un nombre de champs séparés par un séparateur (espace par défaut) pouvant être modifié par l'option ``-F`` ou changement de la variable ``FS`` interne.  
Ici, les variables utilisées seront des variables internes à ``awk`` &rightarrow; non-accessibles depuis le shell.  
Avec ``awk`` une ligne complète est contenue dans la variable ``$0`` et les différents champs contenus dans ``$1, $2, $3``..., cf exemple p.178.  

``print`` est un mot-clef spécifique à ``awk`` permettant d'afficher des variables.  

``awk`` permet d'effectuer des calculs sur les champs, cf exemple p.178  

En plus de traiter le.s fichier.s ligne par ligne, ``awk`` permet de spécifier des blocs de pré/post-traitement. Ils sont exécutés avant le traitement de la 1e ligne et après le traitement de la dernière.  
Syntaxe: cf p.178  

La variable ``NR``est une variable prédéfinie de ``awk`` contenant le numéro de la ligne en cours d'analyse &rightarrow; son utilisation dans le bloc ``END`` permet le renvoi du n° de la dernière ligne analysée. Remplacement par sa valeur à l'affichage.  

#### La commande sed

La commande ``sed`` (stream editor) est un éditeur de texte non-interactif &rightarrow; permet l'édition du contenu d'un fichier de façon automatisée mais ne permet pas la saisie de texte.  
Reçoit un flux de texte sur sa stdin et produit le résultat sur sa stdout, elle traite un flux de données de taille illimitée en utilisant peu de mémoire &rightarrow; outil rapide pour l'édition complexe de fichier.  

``sed``lit les noms des fichiers indiqués en argument, sinon lit la stdin. L'option ``-e`` indique les traitements à effectuer. Par défaut les fichiers ne sont pas modifiés, écriture du résultat de la transformation sur la stdout. L'option ``-i`` permet l'écriture dans le fichier.  

L'utilisation principale de ``sed`` est la substitution de chaînes de caractères identifiées par une expression régulière de syntaxe: ``s/expression-reguliere/chaine/g``  
``s`` pour "substitute", on souhaite ici remplacer les caractères identifiés par ``expression-reguliere`` par la ``chaine`` donnée. L'option ``g`` à la fin inque que **toutes** les lignes validant l'expression régulière seront traitées, sinon arrêt après la première occurence trouvée.  
L'utilisation de caractères spéciaux tels que ``/`` ou ``&`` nécessite un échappement.  

*Ex:* ``sed 's/alice/bob/g' fichier1 fichier2`` lit le contenu des fichiers 1 et 2 et l'écrit sur la stdout en remplaçant "alice" par "bob". Pour supprimer un mot, remplacement par une chaîne vide: ``sed 's/alice//g' fichier1 fichier2``.  

Il est possible d'utiliser ``sed`` comme un filtre, cf exemple p.179-180  

+ La syntaxe de substitution de ``sed`` permet de spécifier les n° de lignes sur lesquelles elle doit être effectuée, *ex:* ``sed '1,5s/UNIX/*nix/g' fichier`` substitution sur les lignes 1 à 5 incluse
+ On peut également spécifier la substitution sur des lignes selon un motif de recherche, *ex:* ``sed -e '/^C/s/Bash/BASH/g' fichier`` OU ``sed -e '/that/s/Bash/BASH/g' fichier`` remplace "Bash" par "BASH" sur les lignes commençant par "C" OU contenant "that"
+ On peut utiliser des expressions régulières complexes, *ex:* ``sed -e 's/L.*x/XXX/g' fichier`` remplace toutes les chaînes de caractères contenant "L" et "x" avec n'importe quel nombre de caractères entre ces 2 lettres par "XXX"
+ On peut supprimer des lignes trouvées, *ex:* ``sed -i '' -e '/^L*e$/d' fichier`` grâce à l'action de ``-d`` suppression de toutes les lignes commençant par un "L" et terminant par un "x. L'option ``-i ''`` (**avec les quotes**) modifie directement le fichier en argument. ``sed -e '/Linux/d' fichier`` suppression de toutes les lignes contenant "Linux".

La commande ``sed`` permet d'effectuer des modifications complexes sur les fichiers:

+ ``sed -e 's/.*/Je dis: &/' fichier`` préfixe chaque ligne par "Je dis: ", le caractère ``&`` indique à ``sed`` d'insérer la chaîne de caractères trouvée par l'expression régulière. Ici, l'expression régulière ``.*`` sélectionne toute une ligne (groupe de 0 ou + caractère.s)
+ On peut déclarer des régions et les réutiliser, cf p.181 *ex:*:
	* ``sed -e 's/\(.*\)-\(.*\)-\(.*\)/date: \3 \2 \1' date.txt``, dans l'expression régulière les régions sont définies par les parenthèses (avec ``\``), chaque région est séparée par un ``-`` comme dans le fichier. Après définition des régions, elles peuvent être utilisées car attribution d'un n°
	
## Effectuer des calculs numériques

p.171
