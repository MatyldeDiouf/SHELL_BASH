## Aide à l'interaction

#### Édition de la ligne de commande

cf p.80  

+ ``ctrl + u``: couper **l'ensemble de la ligne** se trouvant **avant** le curseur
+ ``ctrl + k``: couper **l'ensemble de la ligne** se trouvant **après** le curseur
+ ``ctrl + y``: coller ce qui a été coupé/copié
+ ``alt + backspace``: couper le **mot** se trouvant **après** le curseur (jusqu'au mot suivant)
+ ``alt + D``: couper le **mot** se trouvant **avant** le curseur (jusqu'au mot suivant

#### Historique des commandes

L'historique des 500 dernières commandes est conservé dans le fichier ``.bash_history`` du répertoire personnel, il est écrit dans le fichier à la fermeture de la session.  
La commande ``history`` liste les commandes saisies, de la plus ancienne à la plus récente. En ajoutant l'option ``n``, affichage des ``n`` dernières commandes.  

Manipulation historique, cf p.81  

#### Expansion de l'historique

Deux ``!!`` permettent de rappeler la dernière commande exécutée, utile par exemple pour effectuer une commande déjà tapée avec les droits du super utilisateur, *ex:* ``sudo !!``  

Rappel de commande ou d'arguments, cf p.82-83-84  

``:s`` pour "substitute", permet entre autre de rappeler une commande en modifiant un de ses éléments par un autre selon la syntaxe ``!!:s/ancienelement/nouvelelement/``  
*Ex:*  
``wc fichier1.txt``  
``!!:s/1/2/`` renvoie le nombre de lignes, mots et caractères de ``fichier2.txt``.  
La substitution n'a lieu **que pour la première occurence** de l'élément à remplacer, rajouter ``g`` à la fin de la commande permet de substituer **toutes les occurences**.  

cf résumé p.85-86  

#### Auto-complétion et autres raccourcis

cf p.96-97  

## Abréviations pour le nom des fichiers

"pathname expansion" = substitution de nom de fichier, utilise des caractères spéciaux.  
Une phase d'analyse synthétique est effectuée avant chaque exécution de commande pour interpréter les caractères spéciaux.  
Quand l'argument de commande est une expression de la substitution du nom de fichier, on parle de *motif* (ou *pattern*).  

#### Caractères spéciaux et substitution de nom de fichier

+ ``~``: répertoire personnel
+ ``..``: répertoire parent
+ ``.``: répertoire courant

Si le caractère ``~`` n'est pas immédiatement suivi du caractère ``/``, il désigne le **répertoire parent du répertoire personnel**: ``~matylde`` désigne ``/home/matylde``.  

**Pattern matching:**

*Globbing*: tout nom de fichier contenant un caractère spécial est remplacé sur la ligne de commande par la liste de tous les fichiers du répertoire courant correspondant au motif, c'est la *substitution du nom de fichier*.  

+ ``*`` remplace n'importe quelle séquence de caractères, même vide (la portée de la substitution de ``*`` se limite à 1 niveau de l'arborescence du système de fichiers)
+ ``?`` remplace exactement 1 caractère:
	* *ex:* ``echo fichier?.txt`` affichera ``fichier1.txt fichier2.txt`` mais pas ``fichier12.txt``

Les fichiers cachés sont préfixés par un ``.`` et ce dernier ne pourra pas être substitué par un caractère spécial.  

+ ``[]`` remplace 1 caractère contenu dans le motif précisé entre crochets:
	* ``echo [aef]*`` affichera tous les fichiers dont le nom commence par a-, e- ou f-
	* ``echo [A-Z]*[0-9]`` affichera tous les fichiers dont le nom commence par 1 majuscule et termine par 1 chiffre
	
Si on précède les caractères entre crochets d'un ``!``, on exclut tous les caractères du motif, *ex:* ``echo [!A-Z]*`` affichera tous les fichiers dont le nom ne commence pas par une majuscule.  

Classes de caractères, cf p.92-93  

Extended globbing, cf p.94-95  

## Constructions syntaxiques

#### Substitution de variable

*Variable:* espace de stockage en mémoire identifié par un nom, il sert à enregistrer une valeur.  
En bash, une variable ne peut prendre qu'un type de valeur: la chaîne de caractères (absence de typage) &rightarrow; la variable n'a pas besoin d'être déclarée avant son utilisation.  
L'affectation d'une valeur à une variable utilise le symbole ``=`` **sans laisser d'espace autour du caractère égal**, ``nom_de_la_variable=valeur``.  
Un nom de variable ne peut contenir que des caractères alphanumériques (A-Z, a-z, 0-9) et un underscore.  

Pour voir la valeur d'une variable, utilisation du préfixe ``$`` avant le nom de la variable.  

L'accès à une variable n'ayant pas subi d'affectation préalable ne renvoie pas d'erreur mais la valeur vide (ou *null value*). Attribution de la valeur vide à une variable: ``smthg=""``.  
``unset smthg``: suppression de la variable ``smthg``  
``set -u``: configuration du bash permettant d'afficher une erreur si rencontre une variable non-déclarée (n'a pas lieu si la variable est déclarée explicitement avec une valeur vide).  

``${#variable}``: pour obtenir la longueur de la chaîne de caractères de la valeur de ``variable``.  

Il existe 2 façons de gérer l'accès à une variable quand elle n'est pas définie:

+ utilisation de ``set -u`` &rightarrow; erreur d'exécution en changeant comportement par défaut du bash
+ indication d'une valeur par défaut à retourner, se fait avec la substitution de variable avec valeur par défaut: ``${variable:-valeur}``
	* *Ex:* ``echo ${qqchose:-rien}`` renverra ``rien``

On peut substituer des motifs à la valeur d'une variable à l'aide de ``${variable/motif/nouveau}``. La **première occurence** du motif dans la valeur sera changée par le nouveau:  
``var=debut-milieu-fin``  
``echo ${var/-/_}``  
``debut_milieu-fin``  
Pour appliquer la substitution à **tous les motifs**, il faut doubler le premier ``/``:  
``${variable//motif/nouveau}``  

Substitution de:

+ Préfixe: ``${variable#motif}``, supprime le préfixe correspondant au motif de la valeur, si le ``#`` est doublé, c'est le plus grand préfixe qui est supprimé:
	* *Ex:* ``filename=/home/alice/fich.txt``  
		``echo ${filename#/*/}``  
		``alice/fich.txt``  
		``echo ${filename##/*/}``  
		``fich.txt``
+ Suffixe: ``${variable%motif}``, idem mais avec le suffixe
+ Sous-chaînes: ``${variable: position:longueur}``, de la position indiquée le nombre de caractères est extrait (affichage de la string extraite) de la valeur de la variable **sans oublier l'espace**:
	* *Ex:* ``var=debut-milieu-fin``  
		``echo ${var: 6:6}``  
		``milieu``  
		``echo ${var: 6}``  
		``milieu-fin``  
		``echo ${var:-3}``  
		``fin``  
		
Si la longueur n'est pas indiquée, extraction jusqu'à la fin de la chaîne de caractères. Si la position est négative, la position commence à partir de la fin de la chaîne de caractères (à -1).

#### Substitution de commande

*Substitution de commande*: consiste à remplacer une commande par son résultat, s'écrit à l'aide de la construction ``$(command)`` ou ``command`` est le nom de la commande devant être remplacée.  

Le shell interprète les caratères suivants comme des séparateurs (au même titre que l'espace): ``tab``, ``|``, ``&``, ``;``, ``(``, ``)``, ``<``, ``>`` &rightarrow; il est inutile de placer des espaces autour de ces caractères.  

La substitution de commande permet de donner comme argument à une commande le résultat d'une autre, *ex:*  
``cd``  
``echo ici $(pwd)``  
``ici /home/alice``  
La commande ``pwd`` est exécutée en premier et remplacée par son résultat.  

La substitution de commande peut également s'utiliser avec l'affectation pour indiquer une valeur, *ex:*  
``rep=$(pwd)``  
``echo $rep``  
``/home/alice``  
La variable ``rep`` contient le chemin d'accès au répertoire courant.  

Il n'y a pas de contrainte sur le résultat produit par une substitution de commande, *ex:*  
``names=$(cat lipsum.txt)`` &rightarrow; les commandes produisant des résultats sur plusieurs lignes peuvent également être substituées.  

cf exo p.103 pour subtilités vocabulaire (substitution de variable vs. de commande).  

#### Inhibitions

*Inhibition*: sert à contrôler l'interprétation des caractères spéciaux et substitutions faites par le shell ("quoting"), il en existe 3 formes:  

+ ``\``: inhibition d'un caractère, le caractère suivant est pris littéralement
+ ``" "``: inhibition partielle, la substitution de variable/commande et l'inhibition de caractère sont maintenues
+ ``' '``: inhibition totale, la chaîne de caractères est prise littéralement

*Inhibition totale:*  

Elle empêche toutes les interprétations et est notée entre 2 simples quotes. Elle permet de prendre de manière littérale des caractères spéciaux dans une string, *ex:* l'affichage de la string  
``* $(pwd)`` s'obtient par la commande ``echo '* $(pwd)`` &rightarrow; aucune interprétation n'est effectuée.  

*Inhibition partielle:*

Notée entre 2 double quotes, elle autorise les substitutions de variables/commandes et interprétations du caractère spécial ``\``. L'expansion de caractères spéciaux pour les noms de fichiers et l'espace comme séparateur de mots ne sont plus opérants.  

+ *Ex:*  
	``a=*``  
	``echo "$a"``  
	``*``  
On définit une variable ``a`` contenant le caractère spécial ``*``, l'affectation s'effectue **sans interpréter le caractère spécial** &leftarrow; en affichant le contenu de ``a`` en inhibition partielle, la substitution de la variable est appliquée mais pas l'interprétation des noms de fichiers &rightarrow; affichage de ``*``.  

Les espaces sont des séparateurs, pris à l'intérieur de ``" "`` ou ``' '`` ils sont interprétés littéralement &rightarrow; leur nombre est ainsi conservé, *ex:*  

+ ``echo :  :`` renvoie ``: :`` &rightarrow; appel de la commande avec 2 arguments, 1 seul espace est conservé
+ ``echo ":  :"`` renvoie ``:  :`` &rightarrow; l'inhibition partielle fait que la commande est appelée avec 1 seul argument, les 2 espaces sont conservés (affichage de la chaîne de caractères)

Les arguments de la commande ``echo`` sont quasiment toujours à encadrer par les doubles quotes (sauf en cas de substitution de nom de fichier), cf p.105.  
Lorsqu'un argument est attendu et qu'il est rendu par une substitution de variable, si la variable contient la valeur vide alors il y aura le vide à la place de l'argument, *ex:*  
+ ``set --`` permet de traiter les arguments d'une commande, ``$#`` est la variable du comptage d'arguments et ``;`` est un séparateur de commande:  
	``qqchose=""``  
	``set -- $qqchose ; echo $#``  
	``0``  
	``set -- "$qqchose" ; echo $#``  
	``1``  
Dans le 1er cas, rien n'est rendu par la substitution de la variable alors que dans le 2nd, la substitution de la variable ne rend toujours rien mais prend la signification de "valeur vide".  

Inhibition de caractères ou échappement, notée à l'aide de ``\`` préfixant le caractère à inhiber, *ex:* ``echo \$a`` renverra ``$a``.  
L'inhibition de l'espace est utilisée pour supprimer sa fonction de séparateur et l'inhibition de la fin de ligne permet d'empêcher la terminaison de la ligne de commande, cf p.106.  

Ordre de priorité, cf tableau p.116  

Le shell procède à l'interprétation de la ligne de commande en effectuant les opérations dans l'ordre suivant:

1. Substitution de variable
2. Substitution de commande
3. Substitution de noms de fichier
Pour changer cet ordre ou appliquer 2 fois de suite le même type de substitution, utilisation de la commande interne ``eval``, *ex:*  
	``x=100 ; ptr=x``  
	``echo $ptr``  
	``x``  
	``echo \$$ptr``  
	``x``  
	``eval echo \$$ptr``  
	``100``  
	
Illustration avec cas de figure où la substitution de commande a lieu avant la substitution de variable:  
	``cmd=pwd``  
	``\$$(echo cmd)``  
	``-bash: $cmd: command not found``  
	``eval \$$(echo cmd)``  
	``/home/alice``  
Ici, la substitution de variable est inhibée afin que la première évaluation effectue uniquement la substitution de commande (``$(echo cmd)``, cf syntaxe substitution commande). Au terme de cette évaluation, il reste ``$cmd`` à évaluer. ``eval`` effectue la substitution de variable et appelle la commande ``pwd`` contenue dans la variable ``cmd`` (on obtiendrait le même résultat avec ``$cmd``).  
cf exos chapitre  

## Contrôler l'exécution des commandes

Après analyse de la ligne de commande, le shell a 2 possibilités:  
+ Lancer lui-même la commande, on parle alors de **commande interne**
+ Lancer l'exécution du programme correspondant, on parle de **commande externe**. À une commande externe correspond un fichier exécutable ayant comme nom le nom de la commande et son exécution entraîne la création d'un processus exécutant le programme correspondant à la commande

#### Notion de processus

Un processus correspond à une instance d'une commande (fichier contenant un programme) en exécution, son activité, de sa vie à sa mort, est gérée par l'OS. Le processus est constitué par le code du programme et par les données (variables par ex) qu'il manipule.  
Il est décrit dans le système par un contexte d'exécution dans lequel sont pris en compte l'état d'avancement de son exécution, les ressources utilisées, les droits associés et ses attributs. L'OS gère plusieurs processus en même temps.  

cf p.113-114  

Notions de processus parent (créateur) et processus fils (créé). Le processus parent attend la fin du fils, qui doit faire ``exit()`` pour se terminer. Le processus fils créé remplace le code et les données du père, le père n'exécute pas la commande lui-même pour qu'en cas d'erreur il ne soit pas affecté et puisse la contrôler. Quand le processus père meurt il tue tous les fils.  

L'état de fin d'un processus est matérialisé par un code retour disponible au processus parent qui lui indique comment il s'est terminé. L'affichage du code retour se fait à l'aide de ``echo $?``, selon la valeur retournée on sait comment s'est déroulé la commande:  
+ ``0``: bon déroulement de la commande, terminaison correcte du processus
+ ``&ne; 0``: situation anormale, l'exécution de la commande a rencontré une erreur

Principaux attributs d'un processus, cf p.115, la commande pour afficher ces informations est ``ps``, cf p.115 pour les différentes options.  

États d'un processus:  
+ *sleeping* (noté S): processus en attente d'un évènement
+ *disk sleep* (noté D): quasi similaire à S mais concernat les opérations entrées-sorties
+ *running* (noté R): processus en traitement par la CPU
+ *stopped* (noté T): processus suspendu temporairement, pourra être redémarré sur demande explicite

`Possibilité de spécifier un format avec l'option ``-o``: ``ps -o user,s,pid,pcpu,cmd``.  
Mise à jour en temps réel à l'aide de la commande ``top``.  
Évaluation de la performance de l'exécution d'une commande avec statistiques temporelles de fin d'exécution avec la commande ``time`` qui précèdera la commande à évaluer, *ex:* ``/usr/bin/time sleep 10``  

La commande ``tmux`` est un multiplexeur de terminal, permet d'exploiter plusieurs terminaux virtuels au sein d'une même console. Cf p.117 combinaisons.  

#### Les tâches d'exécutions

*Fonctionnalité de gestion de processus (Job control)*: lorsqu'un programme n'a pas besoin d'interagir avec le terminal, l'exécuter en parallèle comme un processus d'arrière plan &rightarrow; le terminal n'est pas bloqué par son exécution. Par opposition, on parle de commandes d'avant plan.  
On ne parle alors plus de processus mais de **tâches d'exécutions** pour ces processus sous contrôle de l'utilisateur via son shell. Une tâche peut être constituée par un groupe de processus comme pour les commandes s'enchaînant sur une même ligne de commande.  

Lancer commande en arrière plan avec ``&``, *ex:* ``sleep 10 &``, affichage du n° de tâche entre ``[]`` puis du n° de processus.  
Commande interne ``jobs`` liste les tâches d'arrière plan en cours d'exécution. Les n° de tâche et de processus sont gérés par le shell (et non par l'OS). Le n° est un N° de séquence et non un identifiant, *ex:* si la tâche 4 s'arrête, la nouvelle tâche activée portera le n°4.  

Ramener une tâche en avant-plan (foreground) à l'aide la commande interne ``fg %1``, le caractère ``%`` indiquant le n° de la tâche.  
Ramener une tâche en arrière-plan (background) &rightarrow; mise en exécution en mode multi-tâches alors que en cours d'exécution, en 2 étapes:  

+ suspension de la commande à l'aide de ``ctrl + z``
+ envoi en arrière plan à l'aide la commande interne ``bg`` (vérification éventuelle avec ``jobs``)

#### Contrôle d'exécution

Suspension/arrêt d'un processus lorsque comportement anormal. Contrôle de l'exécution s'effectue en envoyant des signaux (ordres donnés aux processus). Envoi à l'aide de la commande ``kill``:  
``kill [-``*nom du signal* | *numéro de signal*``]``*PID*...  
Liste des signaux disponibles à l'aide de ``kill -l``, la liste indique le n° associé à chaque nom de signal, cf liste p.129, *ex:* ``kill -15 96`` signifie l'utilisation de la commande ``kill`` avec le signal de terminaison 15 (= SIGTERM) pour le processus ayant comme PID 96.  
Lorsque c'est une taĉhe qui doit être interrompue, on remplace en argument de la commande le PID par le n° de tâche.  

Par défaut, ``kill`` envoie le signal "SIGTERM" de terminaison de processus à ceux dont le PID est donné en argument.  

Redémarrer une tâche arrêtée: ``kill -CONT %1`` puis passage en avant-plan: ``fg %%`` (``%%`` = tâche courante).  

La commande ``killall`` est équivalente à ``kill`` mais tuera tous les processus dont le nom correspondra à celui donné en argument.  

``ctrl + c`` envoie le signal d'interruption SIGINT, ``ctrl + z`` envoie le signal STOP.  

Attribution d'une priorité à un processus, se fait sous forme d'incrément à l'aide de la commande ``nice``. (plus prioritaire) -20 < priorité < 19 (moins prioritaire), *ex:*  
``nice -n 15 sleep 10 &``.  

Le shell étant le processus parent de tous les processus d'un utilisateur, sa terminaison à la fermeture de la session termine tous les processus fils, envoi du signal SIGHUP à tous les processus fils.  
La commande ``nohup`` rend insensible aux signaux INT et HUP &rightarrow; détache un processus d'arrière plan du shell. Utile en cas de programme à traitement long nécessitant que le traitement continue après que sa session se termine.  
*Ex:* ``nohup monpgm &``, lancement du programme ``mpgm`` en mode détaché.  

## Entrées et sorties des processus

#### Enchaînement de commandes

Exécution séquentielle à partir de la même ligne de commande, si absence de relation entre les commandes séparation à l'aide de ``;`` qui lancera la 2de commande lorsque la 1e sera terminée.  
En cas de condition sur l'exécution pour l'enchaînement de commandes, dépend du code retour de la 1e commande et utilisation des opérateurs ``&&`` et ``||``.  

**Mécanisme de redirection:** en cas de dépendance des données, les données produites par une commande servent de données d'entrée à une autre. Utilisation possible d'un fichier intermédiaire avec écriture du résultat de la 1e commande dans le fichier puis utilisation de ce fichier comme entrée pour la 2de.  
**Tube ("pipe"):** possibilité de branchement de commandes, traitement des données au fur et à mesure de la lecture des données d'entrée.  

cf schéma p.134, un processus utilise des canaux pour lire/écrire des données.  
+ *stdin, canal 0* (standard input): associée au clavier, canal pour la lecture des données
+ *stdout, canal 1* (standard output): associée à l'écran du terminal, canal pour l'écriture des données
+ *stderr, canal 2* (standard error): associée à l'écran du terminal, réception des messages d'erreurs de la commande
Ces 3 canaux peuvent être associés à des fichiers texte, on parle de "redirection des entrées-sorties".  

#### Les redirections

Les redirections des entrées-sorties permettent l'utilisation des fichiers de texte en stdin et stdout plutôt que clavier et écran, le résultat d'une commande est conservé dans un fichier.  

**Sortie standard / erreur standard:**  

2 modes de redirection:  

+ ``> fichier``: écrit la sortie de la commande dans ``fichier``, sera écrasé s'il existe
+ ``>> fichier``: ajoute la sortie de la commande à la fin de ``fichier``, sera créé s'il n'existe pas

Une commande possède une deuxième sortie, reçevant les messages d'erreurs écrits par la commande. La redirection de ce canal se note ``2>`` (sans espace), *ex:* si ``fich1`` n'existe pas, envoi de la stderr générée par la commande dans un fichier log: ``ls fich1 2>log.txt``.  
(Idem avec ``>>``)  

``/dev/null`` = fichier particulier dans lequel tout ce qui est écrit est ignoré.  
Ignorer l'affichage d'un message d'erreur &rightarroww; redirection de la stderr dans le fichier ``/dev/null``, *ex:* ``cat fich1 2>/dev/null`` &rightarrow; pas d'affichage de message d'erreur.  

Possibilité de redirection d'un canal vers un autre, ``1>&2`` (ou ``>&2``): le canal de n°1 est redirigé vers le canal de n°2, la stdout s'affiche dans la stderr. Utile pour afficher des messages d'erreur, cf p.136.  

Redirection des stdout et stderr dans un même fichier par une redirection des canaux:  
``2>fichier 1>&2``.  
Dans cette opération, le nom du fichier en écriture n'apparaît qu'une seule fois.  
On indique la redirection d'un canal vers le fichier puis redirection de l'autre canal vers le canal redirigé préalablement, l'ordre des redirections est important c'est pourquoi le shell propose l'écriture simplifiée ``&>fichier``, cf p.136.  

``set -o noclobber`` (remise du mode écrasement avec ``+o``): en cas de tentative d'écrasement d'un fichier existant, la redirection en sortie mettra la ligne de commande en erreur. ``>|`` pour écraser explicitement le fichier, *ex:* ``ls >|result.txt``.  

**Entrée standard:**  

Trouve sa principale fonctionnalité dans l'automatisation de scripts, utilisation du symbole ``<``.  
*Ex:* la commande ``wc`` effecue des comptages sur le contenu d'un fichier, en l'absence d'argument elle s'effectue sur l'entrée standard (donc le clavier), cf exemple p.137.  
À la place du clavier, possibilité d'alimenter l'entrée par un fichier en redirigeant l'entrée standard: ``wc -l <log.txt`` (même sortie, sans nom de fichier, que ``wc -l log.txt``).

La redirection de l'entrée standard avec ``<<`` indique à une commande que tout ce qui suit ``<<`` doit être pris comme entrée de la commande jusqu'à la rencontre d'une étiquette. Cette étiquette marquant la fin de l'entrée est indiquée après les chevrons, *ex:*  
	``wc -l <<FIN``  
	``un``  
	``deux``  
	``trois``  
	``FIN``  
	``4``  
L'entrée ne provient pas du clavier, les données en entrée sont lues directement sur la ligne de commande. Cf p.138.  

#### Branchement de commandes

Possibilité de branchements de commandes à la suite pour effectuer des traitements spécifiques: la sortie d'une commande sert d'entrée à la suivante sans fichier intermédiaire grâce à un pipe.  
Avec un pipe, les commandes branchées créent des processus existants simultanément, ils s'exécutent de manière synchronisée et non les uns après les autres (traitement en flux).  
Toutes les commandes branchées s'exécutent en même temps en traitant le flux de données au fur et à mesure qu'il est produit, cf exemple p.139.  

Le branchement n'est utile que pour les commandes capables de lire et d'écrire depuis les canaux standards, cf exos partie.  

